package vn.edu.fpt.ultrahealth;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;

import java.util.List;

import vn.edu.fpt.ultrahealth.ExerciseModel;

public class newAdapter extends ArrayAdapter<ExerciseModel> {

    private Context context;
    private List<ExerciseModel> exerciseModels;


    public newAdapter(Context context, List<ExerciseModel> exerciseModelList) {
        super(context, R.layout.custom_list_ex, exerciseModelList);

        this.context = context;
        this.exerciseModels = exerciseModelList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_list_ex, null, true);
        TextView exname = view.findViewById(R.id.exname);
        ImageView eximg = view.findViewById(R.id.eximg);

        exname.setText(exerciseModels.get(position).getName());
        Glide.with(context).load(exerciseModels.get(position).getUrl()).into(eximg);

        return view;
    }
}
