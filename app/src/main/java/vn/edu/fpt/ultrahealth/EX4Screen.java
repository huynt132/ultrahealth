package vn.edu.fpt.ultrahealth;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import java.util.Locale;

public class EX4Screen extends AppCompatActivity {

    Ex3Activity ex3 = new Ex3Activity();
    private static final long START_TIME_IN_MILLIS = 300000;
    private TextView mTextViewCountDown;
    private Button mButtonStartPause;
    private CountDownTimer mCountDownTimer;
    private boolean mTimerRunning;
    private long mTimeLeftInMillis = START_TIME_IN_MILLIS;
    private FirebaseDatabase db;
    private DatabaseReference ref;
    pl.droidsonroids.gif.GifImageView imageView;
    int timeTerm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ex4_screen);
//        getSupportActionBar().setTitle("Schedule");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);

        db = FirebaseDatabase.getInstance();
        ref = db.getReference();
        mTextViewCountDown = findViewById(R.id.txtCountTime);
        mButtonStartPause = findViewById(R.id.Btn_StartStop);

//        ExerciseModel newe = new ExerciseModel("Abdominal Crunch","https://i.imgur.com/UJAnRhJ.gif","",0);
//        ref.child("ex").child("saturday").child(String.valueOf(newe.getName())).setValue(newe);

//        ref.child("ex").child("sunday").child("AbdominalCrunch").get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
//            @Override
//            public void onComplete(@NonNull Task<DataSnapshot> task) {
//                if (task.isSuccessful()) {
//                    Object obj = task.getResult().getValue();
//                    imageView = findViewById(R.id.videoView);
//                    Glide.with(EX4Screen.this).load("https://i.imgur.com/UJAnRhJ.gif").into(imageView);
//                }
//            }
//        });

        Intent intent = getIntent();
        int position = intent.getIntExtra("position", 0);
        imageView = findViewById(R.id.videoView);
        Glide.with(EX4Screen.this).load(Ex3Activity.listex.get(position).getUrl()).into(imageView);

        mButtonStartPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mTimerRunning) {
                    pauseTimer();
                } else {
                    startTimer();
                }
            }
        });


        updateTimer();
    }

    public void startTimer(){
        mCountDownTimer = new CountDownTimer(mTimeLeftInMillis, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                mTimeLeftInMillis = millisUntilFinished;
                updateTimer();
            }
            @Override
            public void onFinish() {
                mTextViewCountDown.setText("Exercise Done !!!");
                mButtonStartPause.setVisibility(View.INVISIBLE);
            }
        }.start();
        mTimerRunning = true;
        mButtonStartPause.setText("PAUSE");
    }

    public void pauseTimer(){
        mCountDownTimer.cancel();
        mTimerRunning = false;
        mButtonStartPause.setText("START");

        Intent intent = getIntent();
        int position = intent.getIntExtra("position", 0);
        db = FirebaseDatabase.getInstance();
        ref = db.getReference();
        int count = (int) Ex3Activity.listex.get(position).getExTime();

        String day = ex3.loadDay();
        if (day.equals("tuesday")) day = "tueday";
        if (day.equals("wednesday")) day = "wedday";
        if (day.equals("thursday")) day = "thuday";

        if(count==0){
            ref.child("ex").child(day).child(Ex3Activity.listex.get(position).getName()).child("exTime").setValue(timeTerm);
            Ex3Activity.listex.get(position).setExTime(timeTerm);
        }else{
            ref.child("ex").child(day).child(Ex3Activity.listex.get(position).getName()).child("exTime").setValue(count+timeTerm);
            Ex3Activity.listex.get(position).setExTime(timeTerm);
        }
        Ex3Activity.listex.clear();

    }

    public void updateTimer(){

        int min = (int) (300000-mTimeLeftInMillis/1000)/60 - 4995;
        int sec = (int) (300000-mTimeLeftInMillis/1000) % 60;
        String timeLeft = String.format(Locale.getDefault(),"%02d:%02d",min,sec);
        mTextViewCountDown.setText(timeLeft);
        timeTerm= min*60+sec;


    }


}