package vn.edu.fpt.ultrahealth;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

public class LoginActivity extends AppCompatActivity {

    private Button btnLogin, btnLoginGG;
    private TextView btnSignUp;
    private EditText txtName, txtPass;
    private FirebaseAuth mFirebaseAuth;
    private GoogleSignInClient mGoogleSignInClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txtName = findViewById(R.id.txtName_LoginActivity);
        txtPass = findViewById(R.id.txtPass_LoginActivity);
        btnLogin = (Button) findViewById(R.id.btnLogin_LoginActivity);
        mFirebaseAuth = FirebaseAuth.getInstance();
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = txtName.getText().toString();
                String pass = txtPass.getText().toString();
                if (!email.isEmpty() && !pass.isEmpty()) {

                    mFirebaseAuth.signInWithEmailAndPassword(email, pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in complete
                                startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                            } else {
                                alertDialog("Wrong email or password, please input again");
                                //      Toast.makeText(LoginActivity.this,"Something went wrong",Toast.LENGTH_SHORT).show();

                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            //Toast.makeText(LoginActivity.this,"Error",Toast.LENGTH_SHORT).show();
                            alertDialog("Error");
                        }
                    }).addOnCanceledListener(new OnCanceledListener() {
                        @Override
                        public void onCanceled() {
                            //Toast.makeText(LoginActivity.this,"Cancel",Toast.LENGTH_SHORT).show();
                            alertDialog("Cancel");
                        }
                    })

                    ;

                } else {
                    //Toast.makeText(LoginActivity.this,"Please field all text",Toast.LENGTH_SHORT).show();
                    alertDialog("Please field all text");
                    //alertDialog("Cancel");
                }

            }
        });


        btnSignUp = findViewById(R.id.btnSingUp_LoginActivity);

        createrRequest();
        btnLoginGG = (Button) findViewById(R.id.btnLoginGG_LoginActivity);
        btnSignUp = findViewById(R.id.btnSingUp_LoginActivity);


        btnLoginGG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rsLaucher.launch(new Intent(mGoogleSignInClient.getSignInIntent()));
            }
        });

    }

    private void alertDialog(String mess) {

        TextView txtMess;
        ImageView cancel;
        Button btnOk;

        //will create a view of our custom dialog layout
        View alertCustomdialog = LayoutInflater.from(LoginActivity.this).inflate(R.layout.custom_dialog, null);
        //initialize alert builder.
        AlertDialog.Builder alert = new AlertDialog.Builder(LoginActivity.this);

        //set our custom alert dialog to tha alertdialog builder
        alert.setView(alertCustomdialog);
        cancel = (ImageView) alertCustomdialog.findViewById(R.id.cancel_button);
        btnOk = (Button) alertCustomdialog.findViewById(R.id.btnOK);
        txtMess = (TextView) alertCustomdialog.findViewById(R.id.message);
        txtMess.setText(mess);
        final AlertDialog dialog = alert.create();
        //this line removed app bar from dialog and make it transperent and you see the image is like floating outside dialog box.
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //finally show the dialog box in android all
        dialog.show();
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }
    private void firebaseAuthWithGoogle(String idToken) {
        AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
        mFirebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                           // Log.d(TAG, "signInWithCredential:success");
                            alertDialog("success GG");
                            FirebaseUser user = mFirebaseAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                           // Log.w(TAG, "signInWithCredential:failure", task.getException());
                            alertDialog("failure GG");
                            //Snackbar.make(mBinding.mainLayout, "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                        // ...
                    }
                });
    }
    public void updateUI(FirebaseUser account){

        if(account != null){
            Toast.makeText(this,"You Signed In successfully",Toast.LENGTH_LONG).show();
            startActivity(new Intent(LoginActivity.this,HomeActivity.class));

        }else {
            Toast.makeText(this,"You Didnt signed in",Toast.LENGTH_LONG).show();
        }

    }
    ActivityResultLauncher<Intent> rsLaucher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK){
                        Intent intent =result.getData();
                        Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(intent);
                        try {
                            // Google Sign In was successful, authenticate with Firebase
                            GoogleSignInAccount account = task.getResult(ApiException.class);

                            firebaseAuthWithGoogle(account.getIdToken());
                        } catch (ApiException e) {
                            // Google Sign In failed, update UI appropriately

                        }

                    }



                }
            }
    );



    private void createrRequest() {
        GoogleSignInOptions gso = new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("623171271229-8qpfe0nrkkli9v22c6srtik1mt5j5pp7.apps.googleusercontent.com")
                .requestEmail()
                .build();
        mGoogleSignInClient= GoogleSignIn.getClient(this,gso);

    }



//    private void firebaseAuthWithGoogle(String idToken) {
//        AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
//        mFirebaseAuth.signInWithCredential(credential)
//                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task<AuthResult> task) {
//                        if (task.isSuccessful()) {
//                            // Sign in success, update UI with the signed-in user's information
//
//                            FirebaseUser user = mFirebaseAuth.getCurrentUser();
//                            startActivity(new Intent(LoginActivity.this,HomeActivity.class));
//                            finish();
//                          //  Toast.makeText(LoginActivity.this, "Login Ok", Toast.LENGTH_SHORT).show();
//                            alertDialog("Login with GG");
//                        } else {
//                            // If sign in fails, display a message to the user.
//
//                            //Toast.makeText(LoginActivity.this, "Login Fail", Toast.LENGTH_SHORT).show();
//                            alertDialog("Login fail");
//                        }
//
//                        // ...
//                    }
//                });
//    }

    //direct signup Activity
    public void onLoginClick(View view) {
        Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
        startActivity(intent);
    }

}