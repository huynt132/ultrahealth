package vn.edu.fpt.ultrahealth;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class HealthProfileActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

   private Button btnDatePicker, btnTimePicker,btnSave;
    EditText editTextDOB, txtTime,editTextName,editTextHeight,editTextWeight;
 //   Spinner spinnerGender,spinnerBloodType;
    private int mYear, mMonth, mDay, mHour, mMinute;

    // ...
    private FirebaseDatabase mFirebaseDatabase;
    private  FirebaseAuth mAuth;
    private  FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference databaseReference;



    String[] genders = { "Man","Woman","Both"};
    String[] bloodTypes = { "A","AB","B","O"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_profile);

        //bar
        getSupportActionBar().setTitle("Health Profile");
        getSupportActionBar().setBackgroundDrawable( new ColorDrawable(Color.parseColor("#00ADC1")));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // pick date diaglog
        btnDatePicker=(Button)findViewById(R.id.btn_date);

        editTextDOB=(EditText)findViewById(R.id.editText_dob);
        editTextName=findViewById(R.id.editText_Name);

        editTextHeight=findViewById(R.id.editText_height);
        editTextWeight=findViewById(R.id.editText_Weight);


        btnSave=(Button) findViewById(R.id.btnSave);

        btnDatePicker.setOnClickListener(this);

        //gender
        Spinner spinnerGender = (Spinner) findViewById(R.id.spinner_gender);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, genders);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGender.setAdapter(adapter);
        spinnerGender.setOnItemSelectedListener(this);
        // blood type
        Spinner spinnerBloodType = (Spinner) findViewById(R.id.spinner_Bloodtype);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, bloodTypes);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerBloodType.setAdapter(adapter2);
        spinnerBloodType.setOnItemSelectedListener(this);



        mAuth=FirebaseAuth.getInstance();
        mFirebaseDatabase= FirebaseDatabase.getInstance();
       String userID= mAuth.getCurrentUser().getUid();
      databaseReference= mFirebaseDatabase.getReference("HealthProfile").child(userID);
      healthProfile = new HealthProfile();






        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String    txt_name = editTextName.getText().toString();
                String txt_dob=editTextDOB.getText().toString();
                String txt_gender=spinnerGender.getSelectedItem().toString();
                String txt_height=editTextHeight.getText().toString();
                String txt_weight=editTextWeight.getText().toString();
                String txt_BloodType=spinnerBloodType.getSelectedItem().toString();

                if (editTextName.getText().toString().isEmpty()){
                    Toast.makeText(HealthProfileActivity.this,  "Please add name", Toast.LENGTH_SHORT).show();
                }else{
                    addDataToFirebase( txt_name, txt_dob, txt_gender, txt_height,txt_weight , txt_BloodType);
                }

            }


        });


    }
    HealthProfile healthProfile;
    private void addDataToFirebase(String txt_name, String txt_dob, String txt_gender, String txt_height, String txt_weight, String txt_bloodType) {
    healthProfile.setTxt_name(txt_name);
    healthProfile.setTxt_dob(txt_dob);
    healthProfile.setTxt_gender(txt_gender);
    healthProfile.setTxt_height(txt_height);
    healthProfile.setTxt_weight(txt_weight);
    healthProfile.setTxt_BloodType(txt_bloodType);
     databaseReference.addValueEventListener(new ValueEventListener() {
         @Override
         public void onDataChange(@NonNull DataSnapshot snapshot) {
             databaseReference.setValue(healthProfile);

             Toast.makeText(HealthProfileActivity.this, "add success", Toast.LENGTH_SHORT).show();
         }

         @Override
         public void onCancelled(@NonNull DatabaseError error) {
             Toast.makeText(HealthProfileActivity.this, "add success", Toast.LENGTH_SHORT).show();
             Toast.makeText(HealthProfileActivity.this, "add fail", Toast.LENGTH_SHORT).show();

         }
     });
    }





//    public void writeNewUser(String txt_name, String txt_dob, String txt_gender,String txt_height ,String txt_weight, String txt_BloodType ) {
//        HealthProfile healthProfile= new HealthProfile(txt_name,txt_dob,txt_gender,txt_height,txt_weight,txt_BloodType);
//
//        mDatabase.child("HealthProfile").setValue(healthProfile);
//    }
    //butoon back on bar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public void onClick(View view) {
        if (view == btnDatePicker) {

            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            editTextDOB.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }

    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Toast.makeText(getApplicationContext(), "Selected gender: "+genders[i] ,Toast.LENGTH_SHORT).show();
        Toast.makeText(getApplicationContext(), "Selected blood type:: "+bloodTypes[i] ,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
