package vn.edu.fpt.ultrahealth;

public class HealthProfile {

    private String txt_name,txt_dob,txt_gender,txt_height,txt_weight,txt_BloodType;

    public HealthProfile() {
    }

    public HealthProfile(String txt_name, String txt_dob, String txt_gender, String txt_height, String txt_weight, String txt_BloodType) {
        this.txt_name = txt_name;
        this.txt_dob = txt_dob;
        this.txt_gender = txt_gender;
        this.txt_height = txt_height;
        this.txt_weight = txt_weight;
        this.txt_BloodType = txt_BloodType;
    }

    public String getTxt_name() {
        return txt_name;
    }

    public void setTxt_name(String txt_name) {
        this.txt_name = txt_name;
    }

    public String getTxt_dob() {
        return txt_dob;
    }

    public void setTxt_dob(String txt_dob) {
        this.txt_dob = txt_dob;
    }

    public String getTxt_gender() {
        return txt_gender;
    }

    public void setTxt_gender(String txt_gender) {
        this.txt_gender = txt_gender;
    }

    public String getTxt_height() {
        return txt_height;
    }

    public void setTxt_height(String txt_height) {
        this.txt_height = txt_height;
    }

    public String getTxt_weight() {
        return txt_weight;
    }

    public void setTxt_weight(String txt_weight) {
        this.txt_weight = txt_weight;
    }

    public String getTxt_BloodType() {
        return txt_BloodType;
    }

    public void setTxt_BloodType(String txt_BloodType) {
        this.txt_BloodType = txt_BloodType;
    }
}
