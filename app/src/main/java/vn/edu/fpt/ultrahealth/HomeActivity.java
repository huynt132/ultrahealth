package vn.edu.fpt.ultrahealth;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

import static android.Manifest.permission.CALL_PHONE;

public class HomeActivity extends AppCompatActivity {
    private FirebaseAuth mFirebaseAuth;
    private TextView email;
    private Button btnLogOut;
    private Button btnHealth_Profile, btnCovidInfor, btnWeather, btnFitness, btnCallHotline,btnBMI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mFirebaseAuth = FirebaseAuth.getInstance();
        email = findViewById(R.id.txtEmail_HomActivity);
        btnLogOut = (Button) findViewById(R.id.btnLogOut_HomeActivity);
        btnLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFirebaseAuth.signOut();
                Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

// redirect Health Profile
        btnHealth_Profile = (Button) findViewById(R.id.btnHealth_Profile);
        btnHealth_Profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, HealthProfileActivity.class);
                startActivity(intent);
            }
        });
//redirect Weather
        btnWeather = (Button) findViewById(R.id.btnSubmitEx1);
        btnWeather.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, WeatherActivity.class);
                startActivity(intent);
            }
        });
//redirect  covid infor
        btnCovidInfor = (Button) findViewById(R.id.btnCovidInfor_HomeScreen);
        btnCovidInfor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, CovidChart.class);
                startActivity(intent);
            }
        });
//redirect fitness app
        btnFitness = (Button) findViewById(R.id.btnFitness_HomeScreen);
        btnFitness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, Ex1Activity.class);
                startActivity(intent);
            }
        });
//Call hotline
        btnCallHotline = (Button) findViewById(R.id.btnCallHotline_HomeScreen);
        btnCallHotline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(Intent.ACTION_CALL);
                i.setData(Uri.parse("tel:0612312312"));

                if (ContextCompat.checkSelfPermission(getApplicationContext(), CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                    startActivity(i);
                } else {
                    requestPermissions(new String[]{CALL_PHONE}, 1);
                }
            }
        });
        //BMI
        btnBMI=(Button) findViewById(R.id.btnBMI_LoginActivity);
        btnBMI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, BmiCalculator.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser mFirebaseUser = mFirebaseAuth.getCurrentUser();
        if (mFirebaseUser != null) {
            // there is some user logged in
            email.setText("Hello: " + mFirebaseUser.getEmail());
        } else {
            //no one logged in
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }
    }


}