package vn.edu.fpt.ultrahealth;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class SignUpActivity extends AppCompatActivity {
    private EditText editTextEmail, editTextPass, editTextRePass;

    private Button btnSignUp;
    private FirebaseAuth mFirebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sing_up);
        mFirebaseAuth = FirebaseAuth.getInstance();
        editTextEmail = (EditText) findViewById(R.id.txtMail);
        editTextPass = (EditText) findViewById(R.id.txtPass_SignInActivity);
        editTextRePass = (EditText) findViewById(R.id.txtRePass_SignUpActivity);
        btnSignUp = (Button) findViewById(R.id.btnSignUp_SingUpActivity);


        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = editTextEmail.getText().toString().trim();
                String pass = editTextPass.getText().toString().trim();
                String repass = editTextRePass.getText().toString().trim();
                if (!email.isEmpty() && !pass.isEmpty() && !repass.isEmpty()) {
                    //got values
                    if (pass.equals(repass)) {
                        //pass match
                        if (pass.length() >= 6) {

                            mFirebaseAuth.createUserWithEmailAndPassword(email, pass).
                                    addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {
                                            if (task.isSuccessful()) {
                                                //user created
                                                startActivity(new Intent(SignUpActivity.this, HomeActivity.class));
                                                finish();
                                            } else {
                                                //Something went wrong
                                                //Toast.makeText(SignUpActivity.this,"Email already used",Toast.LENGTH_SHORT).show();
                                                alertDialog("Email already used");
                                            }
                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    //Failed
                                    alertDialog("Error");
                                    //    Toast.makeText(SignUpActivity.this,"Error",Toast.LENGTH_SHORT).show();

                                }
                            }).addOnCanceledListener(new OnCanceledListener() {
                                @Override
                                public void onCanceled() {
                                    //Toast.makeText(SignUpActivity.this,"Cancel try again",Toast.LENGTH_SHORT).show();
                                    alertDialog("Cancel try again");

                                }
                            })
                            ;
                        } else {
                            alertDialog("pass must be 6 character or more");
                            //    Toast.makeText(SignUpActivity.this,"pass must be 6 chart or more",Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        //pass not match
                        //  Toast.makeText(SignUpActivity.this,"Pass not match",Toast.LENGTH_SHORT).show();
                        alertDialog("Pass not match");
                    }

                } else {
                    //mo fied can be left blank
                    alertDialog("Please field all  ");
                    //Toast.makeText(SignUpActivity.this,"Plesea fied all",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void alertDialog(String mess) {
//        AlertDialog.Builder dialog=new AlertDialog.Builder(this);
//        dialog.setMessage(mess);
//        dialog.setTitle("Dialog Box");
//        dialog.setPositiveButton("Back",
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog,
//                                        int which) {
//                        Toast.makeText(getApplicationContext(),"Yes is clicked",Toast.LENGTH_LONG).show();
//                    }
//                });
//
//        AlertDialog alertDialog=dialog.create();
//        alertDialog.show();
        TextView txtMess;
        ImageView cancel;
        Button btnOk;

        //will create a view of our custom dialog layout
        View alertCustomdialog = LayoutInflater.from(SignUpActivity.this).inflate(R.layout.custom_dialog, null);
        //initialize alert builder.
        AlertDialog.Builder alert = new AlertDialog.Builder(SignUpActivity.this);

        //set our custom alert dialog to tha alertdialog builder
        alert.setView(alertCustomdialog);
        cancel = (ImageView) alertCustomdialog.findViewById(R.id.cancel_button);
        btnOk = (Button) alertCustomdialog.findViewById(R.id.btnOK);
        txtMess = (TextView) alertCustomdialog.findViewById(R.id.message);
        txtMess.setText(mess);
        final AlertDialog dialog = alert.create();
        //this line removed app bar from dialog and make it transperent and you see the image is like floating outside dialog box.
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //finally show the dialog box in android all
        dialog.show();
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }
}