package vn.edu.fpt.ultrahealth;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class EX2Screen extends AppCompatActivity {

    Button button_WK;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ex2_screen);

        button_WK = findViewById(R.id.button_WK2);
        button_WK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(EX2Screen.this,Ex3Activity.class));
            }
        });
    }

}