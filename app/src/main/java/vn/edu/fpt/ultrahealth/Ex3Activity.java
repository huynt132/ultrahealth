package vn.edu.fpt.ultrahealth;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Ex3Activity extends AppCompatActivity {

    private ListView rc;
    private newAdapter newAdapter;
    public static List<ExerciseModel> listex;
    private TextView time, calorie;
    private LinearLayout lmon, ltue, lwed, lthu, lfri, lsat, lsun;
    private String dayTerm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ex3);
        rc = findViewById(R.id.listEx);
        time = findViewById(R.id.tvTime);
        calorie = findViewById(R.id.tvCalorie);

        lmon = findViewById(R.id.lmon);
        ltue = findViewById(R.id.ltue);
        lwed = findViewById(R.id.lwed);
        lthu = findViewById(R.id.lthu);
        lfri = findViewById(R.id.lfri);
        lsat = findViewById(R.id.lsat);
        lsun = findViewById(R.id.lsun);

        initUi();
        String day = loadDay();
        getlistexfromdb(day);
        loadBox(day);
        dayTerm = day;
        rc.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(dayTerm.equals(day)){
                    startActivity(new Intent(getApplicationContext(), EX4Screen.class).putExtra("position", position));
                }

            }
        });

        String term;

        lmon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lmon.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lmon.setBackgroundColor(0xF3EBEB);
                ltue.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                ltue.setBackgroundColor(0xF3EBEB);
                lwed.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lwed.setBackgroundColor(0xF3EBEB);
                lthu.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lthu.setBackgroundColor(0xF3EBEB);
                lfri.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lfri.setBackgroundColor(0xF3EBEB);
                lsat.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lsat.setBackgroundColor(0xF3EBEB);
                lsun.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lsun.setBackgroundColor(0xF3EBEB);

                listex.clear();
                getlistexfromdb("monday");
                dayTerm = "monday";
                lmon.getBackground().setColorFilter(Color.parseColor("#E4CD9C"), PorterDuff.Mode.DARKEN);
                lmon.setBackgroundColor(0xE4CD9C);
                loadBox(day);
            }
        });

        ltue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lmon.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lmon.setBackgroundColor(0xF3EBEB);
                ltue.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                ltue.setBackgroundColor(0xF3EBEB);
                lwed.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lwed.setBackgroundColor(0xF3EBEB);
                lthu.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lthu.setBackgroundColor(0xF3EBEB);
                lfri.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lfri.setBackgroundColor(0xF3EBEB);
                lsat.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lsat.setBackgroundColor(0xF3EBEB);
                lsun.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lsun.setBackgroundColor(0xF3EBEB);

                listex.clear();
                getlistexfromdb("tuesday");
                dayTerm = "tuesday";
                ltue.getBackground().setColorFilter(Color.parseColor("#E4CD9C"), PorterDuff.Mode.DARKEN);
                ltue.setBackgroundColor(0xE4CD9C);
                loadBox(day);
            }
        });

        lwed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lmon.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lmon.setBackgroundColor(0xF3EBEB);
                ltue.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                ltue.setBackgroundColor(0xF3EBEB);
                lwed.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lwed.setBackgroundColor(0xF3EBEB);
                lthu.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lthu.setBackgroundColor(0xF3EBEB);
                lfri.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lfri.setBackgroundColor(0xF3EBEB);
                lsat.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lsat.setBackgroundColor(0xF3EBEB);
                lsun.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lsun.setBackgroundColor(0xF3EBEB);

                listex.clear();
                getlistexfromdb("wednesday");
                dayTerm = "wednesday";
                lwed.getBackground().setColorFilter(Color.parseColor("#E4CD9C"), PorterDuff.Mode.DARKEN);
                lwed.setBackgroundColor(0xE4CD9C);
                loadBox(day);
            }
        });

        lthu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lmon.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lmon.setBackgroundColor(0xF3EBEB);
                ltue.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                ltue.setBackgroundColor(0xF3EBEB);
                lwed.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lwed.setBackgroundColor(0xF3EBEB);
                lthu.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lthu.setBackgroundColor(0xF3EBEB);
                lfri.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lfri.setBackgroundColor(0xF3EBEB);
                lsat.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lsat.setBackgroundColor(0xF3EBEB);
                lsun.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lsun.setBackgroundColor(0xF3EBEB);

                listex.clear();
                getlistexfromdb("thuday");
                dayTerm = "thuday";
                lthu.getBackground().setColorFilter(Color.parseColor("#E4CD9C"), PorterDuff.Mode.DARKEN);
                lthu.setBackgroundColor(0xE4CD9C);
                loadBox(day);
            }
        });

        lfri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lmon.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lmon.setBackgroundColor(0xF3EBEB);
                ltue.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                ltue.setBackgroundColor(0xF3EBEB);
                lwed.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lwed.setBackgroundColor(0xF3EBEB);
                lthu.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lthu.setBackgroundColor(0xF3EBEB);
                lfri.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lfri.setBackgroundColor(0xF3EBEB);
                lsat.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lsat.setBackgroundColor(0xF3EBEB);
                lsun.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lsun.setBackgroundColor(0xF3EBEB);

                listex.clear();
                getlistexfromdb("friday");
                dayTerm = "friday";
                lfri.getBackground().setColorFilter(Color.parseColor("#E4CD9C"), PorterDuff.Mode.DARKEN);
                lfri.setBackgroundColor(0xE4CD9C);
                loadBox(day);
            }
        });

        lsat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lmon.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lmon.setBackgroundColor(0xF3EBEB);
                ltue.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                ltue.setBackgroundColor(0xF3EBEB);
                lwed.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lwed.setBackgroundColor(0xF3EBEB);
                lthu.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lthu.setBackgroundColor(0xF3EBEB);
                lfri.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lfri.setBackgroundColor(0xF3EBEB);
                lsat.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lsat.setBackgroundColor(0xF3EBEB);
                lsun.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lsun.setBackgroundColor(0xF3EBEB);

                listex.clear();
                getlistexfromdb("saturday");
                dayTerm = "saturday";
                lsat.getBackground().setColorFilter(Color.parseColor("#E4CD9C"), PorterDuff.Mode.DARKEN);
                lsat.setBackgroundColor(0xE4CD9C);
                loadBox(day);
            }
        });

        lsun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lmon.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lmon.setBackgroundColor(0xF3EBEB);
                ltue.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                ltue.setBackgroundColor(0xF3EBEB);
                lwed.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lwed.setBackgroundColor(0xF3EBEB);
                lthu.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lthu.setBackgroundColor(0xF3EBEB);
                lfri.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lfri.setBackgroundColor(0xF3EBEB);
                lsat.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lsat.setBackgroundColor(0xF3EBEB);
                lsun.getBackground().setColorFilter(Color.parseColor("#F3EBEB"), PorterDuff.Mode.DARKEN);
                lsun.setBackgroundColor(0xF3EBEB);

                listex.clear();
                getlistexfromdb("sunday");
                dayTerm = "sunday";
                lsun.getBackground().setColorFilter(Color.parseColor("#E4CD9C"), PorterDuff.Mode.DARKEN);
                lsun.setBackgroundColor(0xE4CD9C);
                loadBox(day);
            }
        });

    }

    private void loadBox(String day) {
        if (day.equals("monday")) {
            lmon.getBackground().setColorFilter(Color.parseColor("#66BB6A"), PorterDuff.Mode.DARKEN);
        }
        if (day.equals("tuesday")) {
            ltue.getBackground().setColorFilter(Color.parseColor("#66BB6A"), PorterDuff.Mode.DARKEN);
        }
        if (day.equals("thursday")) {
            lthu.getBackground().setColorFilter(Color.parseColor("#66BB6A"), PorterDuff.Mode.DARKEN);
        }
        if (day.equals("wednesday")) {
            lwed.getBackground().setColorFilter(Color.parseColor("#66BB6A"), PorterDuff.Mode.DARKEN);
        }
        if (day.equals("friday")) {
            lfri.getBackground().setColorFilter(Color.parseColor("#66BB6A"), PorterDuff.Mode.DARKEN);
        }
        if (day.equals("saturday")) {
            lsat.getBackground().setColorFilter(Color.parseColor("#66BB6A"), PorterDuff.Mode.DARKEN);
        }
        if (day.equals("sunday")) {
            lsun.getBackground().setColorFilter(Color.parseColor("#66BB6A"), PorterDuff.Mode.DARKEN);
        }

    }

    public String loadDay() {
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date d = new Date();
        String dayOfTheWeek = sdf.format(d);
        return dayOfTheWeek.toLowerCase(Locale.ROOT);
    }

    private void loadTime(List<ExerciseModel> listex) {
        int t=0;
        for (ExerciseModel em : listex) {
            t+= em.getExTime();
        }
        long second = (t) % 60;
        long minute = (t / (60)) % 60;

        time.setText(minute +" minutes");
        calorie.setText(t/7 + "kcal");
    }

    private void initUi() {
        rc = findViewById(R.id.listEx);
        listex = new ArrayList<>();
        newAdapter = new newAdapter(Ex3Activity.this, listex);
        rc.setAdapter(newAdapter);
    }

    public void getlistexfromdb(String day) {
        if (day.equals("tuesday")) day = "tueday";
        if (day.equals("wednesday")) day = "wedday";
        if (day.equals("thursday")) day = "thuday";

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("ex/" + day + "");

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    ExerciseModel exerciseModel = dataSnapshot.getValue(ExerciseModel.class);
                    listex.add(exerciseModel);
                }
                loadTime(listex);
                newAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(Ex3Activity.this, "get list false", Toast.LENGTH_SHORT).show();
            }
        });
    }
}