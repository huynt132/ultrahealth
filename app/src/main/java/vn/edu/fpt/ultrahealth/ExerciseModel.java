package vn.edu.fpt.ultrahealth;

public class ExerciseModel {
    private String name, url, status;
    long exTime;

    public ExerciseModel(String name, String url, String status, long exTime) {
        this.name = name;
        this.url = url;
        this.status = status;
        this.exTime = exTime;
    }

    public ExerciseModel() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getExTime() {
        return exTime;
    }

    public void setExTime(long exTime) {
        this.exTime = exTime;
    }

    @Override
    public String toString() {
        return "ExerciseModel{" +
                "name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", status='" + status + '\'' +
                ", exTime=" + exTime +
                '}';
    }
}
