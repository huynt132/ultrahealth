package vn.edu.fpt.ultrahealth;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.squareup.picasso.Picasso;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class WeatherActivity extends AppCompatActivity {

    private TextView cityName, date, temperate, weather, sunsetTime, sunriseTime, windy;
    private ImageView weatherIcon;
    private ConstraintLayout touchUpdate;
    private boolean isPermissionGranted;

    FusedLocationProviderClient fusedLocationProviderClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        getSupportActionBar().setTitle("Weather Cast");
        getSupportActionBar().setBackgroundDrawable( new ColorDrawable(Color.parseColor("#00ADC1")));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        cityName = findViewById(R.id.tvCity);
        date = findViewById(R.id.date);
        temperate = findViewById(R.id.temperate);
        weather = findViewById(R.id.weather);
        sunsetTime = findViewById(R.id.sunsetTime);
        sunriseTime = findViewById(R.id.sunriseTime);
        windy = findViewById(R.id.windy);
        weatherIcon = findViewById(R.id.weatherIcon);
        touchUpdate = findViewById(R.id.touchUpdate);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        checkPermission();
        getLocation();
        touchUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getLocation();
            }
        });
    }

    private void getLocation() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        fusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
            @Override
            public void onComplete(@NonNull Task<Location> task) {
                String cityName = "";
                Location loc = task.getResult();
                if (loc != null) {
                    Geocoder geocoder = new Geocoder(WeatherActivity.this, Locale.getDefault());
                    try {
                        List<Address> addresses = geocoder.getFromLocation(loc.getLatitude(), loc.getLongitude(), 1);
                        if (addresses.size() > 0) {
                            for (Address adr : addresses) {
                                if (adr.getLocality() != null && adr.getLocality().length() > 0) {
                                    cityName = adr.getLocality();
                                    break;
                                }
                            }
                        }
                        show_weather(cityName);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(WeatherActivity.this, "Pls grant permission", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void show_weather(String cName) {
        String url = "http://api.weatherapi.com/v1/forecast.json?key=3bfb5e158c6143cbba8140554212610&q=" + cName + "&days=1&aqi=yes&alerts=yes";
        RequestQueue requestQueue = Volley.newRequestQueue(WeatherActivity.this);
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    String name = response.getJSONObject("location").getString("name");
                    cityName.setText(name);
                    String temperature = response.getJSONObject("current").getString("temp_c");
                    temperate.setText(temperature + "°C");
                    String wind = response.getJSONObject("current").getString("wind_mph");
                    windy.setText(wind);
                    String wText = response.getJSONObject("current").getJSONObject("condition").getString("text");
                    String wIcon = response.getJSONObject("current").getJSONObject("condition").getString("icon");
                    weather.setText(wText);
                    Picasso.get().load("http:".concat(wIcon)).into(weatherIcon);
                    JSONObject forecastObj = response.getJSONObject("forecast");
                    JSONObject forecastO = forecastObj.getJSONArray("forecastday").getJSONObject(0);
                    JSONObject astro = forecastO.getJSONObject("astro");
                    String sRise = astro.getString("sunrise");
                    String sSet = astro.getString("sunset");
                    sunriseTime.setText(sRise);
                    sunsetTime.setText(sSet);
                    Calendar calendar = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    String f_date = sdf.format(calendar.getTime());
                    date.setText(f_date);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(WeatherActivity.this, volleyError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(jor);
    }

    private void checkPermission() {
        Dexter.withContext(this).withPermission(Manifest.permission.ACCESS_FINE_LOCATION).withListener(new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse) {
                isPermissionGranted = true;
                //show message when permission is granted
                Toast.makeText(WeatherActivity.this, "Permission granted", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onPermissionDenied(PermissionDeniedResponse permissionDeniedResponse) {
                Intent intent = new Intent();
                //forward user to setting for allow permission
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), "");
                intent.setData(uri);
                startActivity(intent);
            }
            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken permissionToken) {
                //keep continue show permission request
                permissionToken.continuePermissionRequest();
            }
        }).check();
    }
    //butoon back on bar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}