package vn.edu.fpt.ultrahealth;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class Ex1Activity extends AppCompatActivity {
Button btnSubmit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ex1);
        btnSubmit=(Button)findViewById(R.id.btnSubmitEx1) ;
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Ex1Activity.this, Ex3Activity.class);
                startActivity(intent);
            }
        });


        //bar
        getSupportActionBar().setTitle("Fitness ");
        getSupportActionBar().setBackgroundDrawable( new ColorDrawable(Color.parseColor("#00ADC1")));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    //butoon back on bar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}