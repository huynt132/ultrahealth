package vn.edu.fpt.ultrahealth;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ExAdapter extends RecyclerView.Adapter<ExAdapter.exView> {

    private List<ExerciseModel> listex;

    public ExAdapter(List<ExerciseModel> listex) {
        this.listex = listex;
    }

    @NonNull
    @Override
    public exView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_list_ex,parent,false);
        return new exView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull exView holder, int position) {
        ExerciseModel ex = listex.get(position);
        if(ex ==null){
            return;
        }
        holder.name.setText(ex.getName());
        holder.time.setText("15 minutes");

    }

    @Override
    public int getItemCount() {
        if(listex != null){
            return listex.size();
        }
        return 0;
    }

    public class exView extends RecyclerView.ViewHolder{

        private TextView name,time;
        private ImageView img;

        public exView(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.exname);
            time =itemView.findViewById(R.id.extime);
            img = itemView.findViewById(R.id.eximg);
        }
    }
}
